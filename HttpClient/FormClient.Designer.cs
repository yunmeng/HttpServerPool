﻿namespace HttpClient
{
    partial class FormClient
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.pEUnload = new System.Windows.Forms.PictureBox();
            this.BtUpload = new System.Windows.Forms.Button();
            this.BtBrower = new System.Windows.Forms.Button();
            this.MyListBox = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pEUnload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pEUnload
            // 
            this.pEUnload.Location = new System.Drawing.Point(26, 28);
            this.pEUnload.Name = "pEUnload";
            this.pEUnload.Size = new System.Drawing.Size(386, 265);
            this.pEUnload.TabIndex = 6;
            this.pEUnload.TabStop = false;
            // 
            // BtUpload
            // 
            this.BtUpload.Location = new System.Drawing.Point(296, 310);
            this.BtUpload.Name = "BtUpload";
            this.BtUpload.Size = new System.Drawing.Size(116, 31);
            this.BtUpload.TabIndex = 5;
            this.BtUpload.Text = "上传";
            this.BtUpload.UseVisualStyleBackColor = true;
            this.BtUpload.Click += new System.EventHandler(this.BtUpload_Click);
            // 
            // BtBrower
            // 
            this.BtBrower.Location = new System.Drawing.Point(26, 310);
            this.BtBrower.Name = "BtBrower";
            this.BtBrower.Size = new System.Drawing.Size(116, 31);
            this.BtBrower.TabIndex = 4;
            this.BtBrower.Text = "浏览";
            this.BtBrower.UseVisualStyleBackColor = true;
            this.BtBrower.Click += new System.EventHandler(this.BtBrower_Click);
            // 
            // MyListBox
            // 
            this.MyListBox.FormattingEnabled = true;
            this.MyListBox.ItemHeight = 12;
            this.MyListBox.Location = new System.Drawing.Point(26, 347);
            this.MyListBox.Name = "MyListBox";
            this.MyListBox.Size = new System.Drawing.Size(828, 136);
            this.MyListBox.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(468, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(386, 265);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(738, 310);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 31);
            this.button1.TabIndex = 9;
            this.button1.Text = "查看图片";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(940, 578);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.MyListBox);
            this.Controls.Add(this.pEUnload);
            this.Controls.Add(this.BtUpload);
            this.Controls.Add(this.BtBrower);
            this.Name = "FormClient";
            this.Text = "Http客户端";
            this.Load += new System.EventHandler(this.FormClient_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pEUnload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pEUnload;
        private System.Windows.Forms.Button BtUpload;
        private System.Windows.Forms.Button BtBrower;
        private System.Windows.Forms.ListBox MyListBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
    }
}

