﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Net.Sockets;
using System.Net;
 
using ClsHttpClient;

namespace HttpClient
{
    public partial class FormClient : Form
    {
        #region 图片互转


        public byte[] ImageToByteArray(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();

        }

        public Image ByteArrayToImage(byte[] byteArrayIn)
        {



            MemoryStream ms = new MemoryStream(byteArrayIn);
            ms.Position = 0;
            Image returnImage = Image.FromStream(ms);

            return returnImage;


        }

        public Image ByteArrayToImage(byte[] byteArrayIn, int count)
        {



            MemoryStream ms = new MemoryStream(byteArrayIn, 0, count);
            ms.Position = 0;
            Image returnImage = Image.FromStream(ms);

            return returnImage;


        }






        #endregion

        ClsHttpClient.ClsHttpClient MyHttpClient = new ClsHttpClient.ClsHttpClient("192.168.1.31", "1983");
   

        
        string PhotoPath = "";  //图片的路径

      
        public FormClient()
        {
            InitializeComponent();
        }


        private void BtBrower_Click(object sender, EventArgs e)
        {
            FileDialog fd = new OpenFileDialog();
            if (fd.ShowDialog() == DialogResult.OK)
            {

                try
                {
                    string path = fd.FileName;
                    PhotoPath = path;
                    string gs = path.Substring(path.LastIndexOf(".") + 1);
                    if (!(gs == "bmp" || gs == "jpg" || gs == "gif" || gs == "jpeg"))
                    {
                        MessageBox.Show("上传图片的格式不正确！请重新选择!标准格式为[bmp jpg gif jpeg]", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    using (System.IO.FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                    {
                        byte[] picbyte = new byte[fs.Length];
                        using(BinaryReader br = new BinaryReader(fs))
                        {
                        picbyte = br.ReadBytes(Convert.ToInt32(fs.Length));
                        pEUnload.Image = ByteArrayToImage(picbyte);

                            }

                    }

                }
                catch (Exception ex)
                {
                
                    MessageBox.Show(ex.Message, "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                 
            }
        }



        #region 取得页

        public string GetPage(string posturl, byte[] data)
        {
            Stream outstream = null;
            Stream instream = null;
            StreamReader sr = null;
            HttpWebResponse response = null;
            HttpWebRequest request = null;
            string content = "";
            Encoding encoding = System.Text.Encoding.GetEncoding("UTF-8");

            // 准备请求...   
            try
            {
                // 设置参数   
                request = WebRequest.Create(posturl) as HttpWebRequest;
                CookieContainer cookieContainer = new CookieContainer();
                request.CookieContainer = cookieContainer;
                request.AllowAutoRedirect = true;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                outstream = request.GetRequestStream();
                outstream.Write(data, 0, data.Length);
                outstream.Close();
                //发送请求并获取相应回应数据   
                response = request.GetResponse() as HttpWebResponse;
                //直到request.GetResponse()程序才开始向目标网页发送Post请求   
                instream = response.GetResponseStream();
                sr = new StreamReader(instream, encoding);
                //返回结果网页（html）代码   
                content = sr.ReadToEnd();
                instream.Close();
                outstream.Close();

                return content;
            }
            catch (Exception ex)
            {
                string err = ex.Message;

                instream.Close();
                outstream.Close();
                MessageBox.Show(err);
            }
            return content;


        }

        #endregion






        private void BtUpload_Click(object sender, EventArgs e)
        {

            DateTime TheDateTime=DateTime.Now;
            string FileName=TheDateTime.ToFileTime()+".jpg";
            try
            {
                if (pEUnload.Image == null)
                {
                    MessageBox.Show("请先选择图片。");
                    return;
                }

                if (MyHttpClient.UploadPhoto(FileName, pEUnload.Image) == "0")
                {
                    MyListBox.Items.Add("图片上传成功!");
                }
                else
                {
                    MyListBox.Items.Add("图片上传失败!");
                }
              
            }

            catch (Exception ex)
            {
               MessageBox.Show(ex.Message);
            } 

   


        }

  


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                
                Image img = MyHttpClient.DownloadPhoto("7758258.jpg");
                if (img != null)
                {
                    pictureBox1.Image = img;
                    MyListBox.Items.Add("查看图片成功。");

                }
                else
                {
                    MyListBox.Items.Add("查看图片失败");
                
                }
             
             
                
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FormClient_Load(object sender, EventArgs e)
        {

        }
    }
}
