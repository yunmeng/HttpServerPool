﻿namespace HttpServerPool
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnListen = new System.Windows.Forms.Button();
            this.MyListBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // BtnListen
            // 
            this.BtnListen.Location = new System.Drawing.Point(68, 60);
            this.BtnListen.Name = "BtnListen";
            this.BtnListen.Size = new System.Drawing.Size(80, 31);
            this.BtnListen.TabIndex = 0;
            this.BtnListen.Text = "监听";
            this.BtnListen.UseVisualStyleBackColor = true;
            this.BtnListen.Click += new System.EventHandler(this.BtnListen_Click);
            // 
            // MyListBox
            // 
            this.MyListBox.FormattingEnabled = true;
            this.MyListBox.ItemHeight = 12;
            this.MyListBox.Location = new System.Drawing.Point(68, 106);
            this.MyListBox.Name = "MyListBox";
            this.MyListBox.Size = new System.Drawing.Size(507, 160);
            this.MyListBox.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 331);
            this.Controls.Add(this.MyListBox);
            this.Controls.Add(this.BtnListen);
            this.Name = "Form1";
            this.Text = "Http服务器";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnListen;
        private System.Windows.Forms.ListBox MyListBox;
    }
}

