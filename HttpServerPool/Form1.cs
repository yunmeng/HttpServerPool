﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

using ClsHttpServerPool;



namespace HttpServerPool
{
    public partial class Form1 : Form
    {
        ClsHttpServerPool.ClsHttpServerPool MyClsHttpServerPool = new ClsHttpServerPool.ClsHttpServerPool(1983);
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnListen_Click(object sender, EventArgs e)
        {
            ThreadStart ts = new ThreadStart(MyClsHttpServerPool.Start); //创建新的线程用于监听
            Thread myThread = new Thread(ts);
            myThread.Start();

            MyListBox.Items.Add("监听成功");

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
    }
}
