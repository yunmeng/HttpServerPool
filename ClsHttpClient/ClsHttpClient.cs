﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Drawing;
using System.IO;
using Common;
namespace ClsHttpClient
{
    public class ClsHttpClient
    {
        
        private string HttpServerIP;
        private string HttpPort;


        private string HttpUrl;


                 
        /// <summary>
        /// HttpClient的构造函数
        /// </summary>
        /// <param name="HttpServerIP">要连接的服务器的IP地址</param>
        /// <param name="HttpPort">要连接的端口号</param>
        public ClsHttpClient(string HttpServerIP, string HttpPort)
        {
            this.HttpPort = HttpPort;
            this.HttpServerIP = HttpServerIP;
            HttpUrl = "http://" + HttpServerIP + ":" + HttpPort + "/";

        }

        /// <summary>
        /// 上传图片到http服务器
        /// </summary>
        /// <param name="FileName">上传到服务器之后存储的图片的文件名,包含扩展名</param>
        /// <param name="ImageBytes">上传图片的字符组</param>
        /// 返回 0表示成功。
        public string   UploadPhoto(string FileName, byte[] ImageBytes)
        {
             string ReturnStr="";

             HttpClsMessage MyHttpClsMessage = new HttpClsMessage();
             MyHttpClsMessage.UploadFileName = FileName;
             MyHttpClsMessage.ComType = HttpCommandType.UploadFile;

            try
            {
                WebClient myClient = new WebClient();
                string  AllHttpUrl = HttpUrl + Common.HttpProtocolHandler.PackMessage(MyHttpClsMessage);
                byte[] ReturnVlaue = myClient.UploadData(AllHttpUrl, ImageBytes); //取得返回值
               ReturnStr = ASCIIEncoding.ASCII.GetString(ReturnVlaue);

            }
            catch (Exception ex)
            {
              
                ReturnStr= ex.Message;
                return ReturnStr;
            }

            return ReturnStr;
 
        
        }

  



        /// <summary>
        /// 上传图片到http服务器
        /// </summary>
        /// <param name="FileName">上传到服务器之后存储的图片的文件名</param>
        /// <param name="Image">要上传的图片</param>
        public string UploadPhoto(string FileName, Image Image)
        {
            return UploadPhoto(FileName, ImageToByteArray(Image));

        }


        /// <summary>
        /// 下载服务器上的图片
        /// </summary>
        /// <param name="FileName">请求的服务器上图片的文件名,要加后缀.jpg</param>
        /// <returns>返回请求的Image对象</returns>
        public Image DownloadPhoto(string FileName)
        {
            Image img=null;

            HttpClsMessage MyHttpClsMessage = new HttpClsMessage();
            MyHttpClsMessage.DownFileName = FileName;
            MyHttpClsMessage.ComType = HttpCommandType.DownloadFile;
            try
            {
                WebClient myClient = new WebClient();
                string AllHttpUrl = HttpUrl + Common.HttpProtocolHandler.PackMessage(MyHttpClsMessage);
                Stream strm = myClient.OpenRead(AllHttpUrl);
                img = Image.FromStream(strm);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                img = null;
                return img;
            }

            return img;
 
           
        }










        #region 图像转换。


        /// <summary>
        /// 把Image对象转成Byte[]
        /// </summary>
        /// <param name="imageIn">传入的Image对象</param>
        /// <returns>返回对应的byte[]</returns>
        private byte[] ImageToByteArray(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();

        }

        /// <summary>
        /// 把byte[]转成Image对象
        /// </summary>
        /// <param name="byteArrayIn">传入的byte[]</param>
        /// <param name="count">传入的byte[]大小</param>
        /// <returns>返回的Image对象</returns>
        private Image ByteArrayToImage(byte[] byteArrayIn, int count)
        {



            MemoryStream ms = new MemoryStream(byteArrayIn, 0, count);
            ms.Position = 0;
            Image returnImage = Image.FromStream(ms);
            return returnImage;

        }



        #endregion







      
    }
}
