#HttpServerPool

大概的流程如下图所示:

![输入图片说明](http://images.cnitblog.com/blog/43700/201301/27111930-eea480367c414c38abb00662bc8a7295.jpg "在这里输入图片标题")

 

1  服务端使用HttpListener类 监听客户端的连接请求。

   HttpListener Listerner = new HttpListener();

 

  服务端新开一个线程，无限循环监听客户端的连接请求。

 

     while (true)

     {

         HttpListenerContext Request=  Listerner.GetContext();  

         ThreadPool.QueueUserWorkItem(ProcessRequest, Request);  

 

      }

 

   Listerner.GetContext()函数在没有连接到来的时候,会挂起当前的线程。

   当有连接到来的时候, 利用线程池,把连接请求抛给ProcessRequest函数处理。

 

     private void ProcessRequest(object listenerContext)

       {

            var context = (HttpListenerContext)listenerContext;

            new ClsProcessRequest().Begin(context);

       

      }

 

   ProcessRequest函数新建一个ClsProcessRequest对象,并调用Begin方法对连接请求进行处理。

 

   ClsProcessRequest类获取客户端传过来的url信息。

   Begin()函数获取URL的信息:

   this.context = context;

   this.context.Response.StatusCode = 200;//设置返回给客服端http状态代码

   DataLen =  Convert.ToInt32(context.Request.ContentLength64);

   filename = Path.GetFileName(context.Request.RawUrl);

  System.Diagnostics.Debug.WriteLine("原始url:" + context.Request.RawUrl);

  System.Diagnostics.Debug.WriteLine("字符总大小:" + DataLen.ToString());

  System.Diagnostics.Debug.WriteLine("客户端的IP:" +context.Request.RemoteEndPoint.ToString());

  System.Diagnostics.Debug.WriteLine("上传的文件名:" + filename);

 

 

 

 http上传下载的请求用以下的函数封装:

 

 public static string  PackMessage(HttpClsMessage MyHttpClsMessage)
        {

 

            string All = Start + "|"

 

                    + MyHttpClsMessage.SNNumber +
                "|" + MyHttpClsMessage.ComType +
                "|" + MyHttpClsMessage.Data +
                "|" +MyHttpClsMessage.DownFileName+
                "|" +MyHttpClsMessage.UploadFileName
                
                 +"|" + End;

 


            return All;

 

        }

 

 

 

 

 

Start变量为:S-T-A-R-T

SNNumber:空字符

ComType:UPLOADFILE为上传图片,DOWNLOADFILE为下载文件

Data:空

DownFileNmae:请求下载的图片文件名

UploadFileName:上传的图片文件名

End变量:E-N-D

 

上传图片:

![输入图片说明](http://images.cnitblog.com/blog/43700/201301/27113658-e5e8e09fdf6b4b51937e41552206e5a6.jpg "在这里输入图片标题")


原始url:/S-T-A-R-T%7C%7CUPLOADFILE%7C%7C%7C130035953323906250.jpg%7CE-N-D

字符总大小:14736

客户端的IP:192.168.1.31:1604

上传的文件名:S-T-A-R-T%7C%7CUPLOADFILE%7C%7C%7C130035953323906250.jpg%7CE-N-D

收到的字符大小:1460

收到的字符大小:8192

收到的字符大小:5084

收到的字符大小:0

发送消息给浏览器。

输入流关闭了。

输出流关闭了。

 

从输出可以看到信息:

是一个上传图片的URL请求，%7C也就是字符 ‘| ‘

 

 上传的图片文件用匿名方法BeginRead读取并写入图片文件中。

  context.Request.InputStream.BeginRead(MyBytes, 0, MyBytes.Length, ReadCallback, null);

  using (Stream stream = new FileStream(SecondSubFolder + MyHttpClsMessage.UploadFileName, FileMode.Append, FileAccess.Write))

                  {

                      //将字符信息写入文件系统

                      stream.Write(MyBytes, 0, ReadSize);

                  }

 

 

下载图片

![输入图片说明](http://images.cnitblog.com/blog/43700/201301/27113720-7fa648c0a3e1461794e74c560e50fc22.jpg "在这里输入图片标题")

原始url:/S-T-A-R-T%7C%7CDOWNFILE%7C%7C7758258.jpg%7C%7CE-N-D

字符总大小:0

客户端的IP:192.168.1.31:3083

上传的文件名:S-T-A-R-T%7C%7CDOWNFILE%7C%7C7758258.jpg%7C%7CE-N-D

 

 

从输出可以看到信息:

是一个下载图片的URL请求，%7C也就是字符 ‘| ‘

 

          using (System.IO.FileStream fs = new FileStream(FullFileName, FileMode.Open, FileAccess.Read))

                  {

                      byte[] picbyte = new byte[fs.Length];

                      using (BinaryReader br = new BinaryReader(fs))

                      {

                          picbyte = br.ReadBytes(Convert.ToInt32(fs.Length));

                          Write(picbyte);

                      }

                  }

 

读取文件流,发送给客户端。

 

-----------------------------------------------------------------------------------------------

 

演示图:



![输入图片说明](http://images.cnitblog.com/blog/43700/201301/27113853-2bac50b86c344efcb9ddd48d8df7a18f.jpg "在这里输入图片标题")



![输入图片说明](http://images.cnitblog.com/blog/43700/201301/27113949-a3536af89c754c60a82b91d929f0c8f3.jpg "在这里输入图片标题")