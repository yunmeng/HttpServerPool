﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
namespace Common
{


    #region 协议

    public class HttpProtocolHandler
    {

       private   static  string Start = "S-T-A-R-T";   //开始符
       private static  string End = "E-N-D";    //结束符


        #region 封包





        public static string  PackMessage(HttpClsMessage MyHttpClsMessage)
        {

            string All = Start + "|"

                    + MyHttpClsMessage.SNNumber +
                "|" + MyHttpClsMessage.ComType +
                "|" + MyHttpClsMessage.Data +
                "|" +MyHttpClsMessage.DownFileName+
                "|" +MyHttpClsMessage.UploadFileName
                
                 +"|" + End;


            return All;

        }




        #endregion


        #region 解包

        public static HttpClsMessage DePackMessage(string Message)
        {


            string MyMessage = Message.Replace("%7C", "|"); //http中。| 是%7C

            HttpClsMessage MyHttpClsMessage = new HttpClsMessage();

            string[] TheMessage = MyMessage.Split('|');
            MyHttpClsMessage.SNNumber = TheMessage[1];
            MyHttpClsMessage.ComType = TheMessage[2];
            MyHttpClsMessage.Data = TheMessage[3];
            MyHttpClsMessage.DownFileName = TheMessage[4];
            MyHttpClsMessage.UploadFileName = TheMessage[5];


            return MyHttpClsMessage;

        }



        #endregion










        private string partialProtocal;	// 保存不完整的协议

        public HttpProtocolHandler()
        {
            partialProtocal = "";
        }

        public string[] GetProtocol(string input)
        {
            return GetProtocol(input, null);
        }

        // 获得协议
        private string[] GetProtocol(string input, List<string> outputList)
        {

            if (outputList == null)                  //如果为空，新建list
                outputList = new List<string>();

            if (String.IsNullOrEmpty(input))     //如果输入的字符串为空
                return outputList.ToArray();

            if (!String.IsNullOrEmpty(partialProtocal))  //如果上次剩下的字符串不为空，加上
                input = partialProtocal + input;

          //  string pattern = "S-T-A-R-T.*?E-N-D";
            string pattern = Start+".*?"+End;

            // 如果有匹配，说明已经找到了，是完整的协议
            if (Regex.IsMatch(input, pattern))
            {

                // 获取匹配的值
                string match = Regex.Match(input, pattern).Groups[0].Value;
                outputList.Add(match);
                partialProtocal = "";

                // 缩短input的长度
                input = input.Substring(match.Length);

                // 递归调用
                GetProtocol(input, outputList); //如果一次发来N条命令

            }
            else
            {
                // 如果不匹配，说明协议的长度不够，
                // 那么先缓存，然后等待下一次请求
                partialProtocal = input;
            }

            return outputList.ToArray();
        }







    }
    #endregion





    #region 消息协议格式类

    public class HttpClsMessage
    {
        public string SNNumber="";
      
        public string ComType="";
        public string Data="";
       public  string DownFileName="";
       public string UploadFileName = "";

    }

    //固定的命令。
    public class HttpCommandType
    {
       public    const string UploadFile = "UPLOADFILE";

       public    const string DownloadFile = "DOWNFILE";



    
    }



    #endregion

}
